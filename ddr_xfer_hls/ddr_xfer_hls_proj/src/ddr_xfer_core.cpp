#include "ddr_xfer_core.h"
#include <string.h>

void ddr_xfer(unsigned int *src_addr, unsigned int dest_addr[4096], int size)
{
	#pragma HLS INTERFACE ap_none port=size
	#pragma HLS INTERFACE bram port=dest_addr
	#pragma HLS INTERFACE m_axi port=src_addr offset=slave bundle=IN_BUS depth=200

	memcpy(dest_addr, src_addr, size * sizeof(int));

}
