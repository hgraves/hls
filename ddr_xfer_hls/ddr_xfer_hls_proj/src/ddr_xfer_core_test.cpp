#include "ddr_xfer_core.h"
#include <iostream>

int main() {

	unsigned int in[1024];
	unsigned int out[1024];

	int ct = 0;
	int length = 640;

	for(int i = 0; i < length; i++)
	{
		in[i] = i + 5;
	}

	ddr_xfer(in, out, length);

	for(int i = 0; i < length; i++)
	{
		if(out[i] == i+5)
		{
			ct++;
		}
	}

	if(ct==length)
		std::cout << "PASS" << std::endl;
	else
		std::cout << "FAIL" << std::endl;

	return 0;
}
