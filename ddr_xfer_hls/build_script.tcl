############################################################
## This file is generated automatically by Vitis HLS.
## Please DO NOT edit it.
## Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
############################################################
open_project ddr_xfer_hls_proj
set_top ddr_xfer
add_files ddr_xfer_hls_proj/src/ddr_xfer_core.cpp
add_files ddr_xfer_hls_proj/src/ddr_xfer_core.h
add_files -tb ddr_xfer_hls_proj/src/ddr_xfer_core_test.cpp
open_solution "solution1" -flow_target vivado
set_part {xczu11eg-ffvc1760-1-i}
create_clock -period 10 -name default
#source "./ddr_xfer_hls_proj/solution1/directives.tcl"
csim_design
csynth_design
#cosim_design
export_design -rtl vhdl -format ip_catalog
